# Introduction
This project is inspired by Prof. Dr. Dr. Martin Haditsch and the current situation of the pandemic, see [this video][1]. This interative app shall give the user a more understanding about the application of the Bayes's theorem in epidemiology, and how we interpreting the test results.

On December 22, 2020, 3Blue1Brown published a precious explanation video about this topic, see [this video][2].

Open the interactive app on this site https://yogiwhy.gitlab.io/medical-test-paradox.

## Donation

If you appreciate my work, you can help contribute to the code or doing a one time donation via Paypal.

[![paypal](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=9H9M4LHP8L8EQ&source=url)

[//]: # (References)
[1]: https://lbry.tv/$/download/narrative-3-livestream-mit-prof-dr-dr/c860a5a37e4c55ff92035534dbd2f8ac9427ca8e
[2]: https://www.youtube.com/watch?v=lG4VkPoG3ko